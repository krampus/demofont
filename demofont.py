#! /usr/bin/env python3
""" A quick script to build a demo file of all codepoints in a TrueType font. """

from fontTools.ttLib import TTFont  # type: ignore
import itertools

import argparse

import typing as T

_DEFAULT_COLUMNS = 1
_COLUMN_MIN_WIDTH = 20
_COLUMN_PADDING = "  "


parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("ttf", type=str, help="TrueType font file")
parser.add_argument(
    "-c",
    "--columns",
    type=int,
    default=_DEFAULT_COLUMNS,
    help=f"Number of columns to print (default: {_DEFAULT_COLUMNS})",
)


def iter_n(iterable: T.Iterable, n: int) -> T.Iterable[T.Tuple]:
    return itertools.zip_longest(*[iter(iterable)] * n)


def column_widths(iterable: T.Iterable[T.Tuple], min_width: int) -> T.Tuple:
    return tuple(
        max(min_width, *(0 if s is None else len(s) for s in col))
        for col in iter_transpose(iterable)
    )


def iter_transpose(iterable: T.Iterable[T.Tuple]) -> T.Iterable[T.Tuple]:
    return zip(*iterable)


def describe_codepoint(codepoint: int, description: str) -> str:
    return f"{chr(codepoint)} [{hex(codepoint)} : {description}]"


def main(args: argparse.Namespace):
    font = TTFont(args.ttf)
    charmap = font.getBestCmap()
    name = font["name"].getBestFullName()

    print(name)
    print()

    descriptions = [describe_codepoint(cp, name) for cp, name in charmap.items()]
    rows = list(iter_n(descriptions, args.columns))
    widths = column_widths(rows, _COLUMN_MIN_WIDTH - len(_COLUMN_PADDING))

    for row in rows:
        line = _COLUMN_PADDING.join(desc.ljust(widths[i]) for i, desc in enumerate(row))
        print(line.strip())


if __name__ == "__main__":
    main(parser.parse_args())
